<?php

namespace KDA\Filament\User;
use Filament\PluginServiceProvider;
use Spatie\LaravelPackageTools\Package;
use KDA\Filament\User\Resources\UserResource;
use Filament\Facades\Filament;
use Filament\Navigation\NavigationBuilder;


class FilamentServiceProvider extends PluginServiceProvider
{
    protected array $styles = [
    //    'my-package-styles' => __DIR__ . '/../dist/app.css',
    ];

    protected array $widgets = [
    //    CustomWidget::class,
    ];

    protected array $pages = [
    //    CustomPage::class,
    ];

    protected array $resources = [
   //     CustomResource::class,
   UserResource::class
    ];
 
    public function configurePackage(Package $package): void
    {
        $package->name('filament-user');
    }

    public function packageBooted(): void{
        parent::packageBooted();

       
    }
}
