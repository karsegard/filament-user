<?php

namespace KDA\Filament\User\Resources;

use KDA\Filament\User\Resources\UserResource\Pages;
use KDA\Filament\User\Resources\UserResource\Pages\CreateUser;
use KDA\Filament\User\Resources\UserResource\Pages\EditUser;
use App\Models\User;
use Filament\Forms;
use Filament\Forms\Components\Tabs;
use Filament\Resources\Form;
use Filament\Resources\Pages\Page;
use Filament\Resources\Resource;
use Filament\Resources\Table;
use Filament\Tables;
use Filament\Tables\Columns\BooleanColumn;
use STS\FilamentImpersonate\Impersonate;
use Filament\Facades\Filament;
use Lab404\Impersonate\Services\ImpersonateManager;

class UserResource extends Resource
{
    protected static ?string $model = User::class;

    protected static ?string $navigationIcon = 'heroicon-o-users';


    protected static function allowed($current, $target): bool
    {
        return $current->isNot($target)
            && !app(ImpersonateManager::class)->isImpersonating()
            && (!method_exists($current, 'canImpersonate') || $current->canImpersonate())
            && (!method_exists($target, 'canBeImpersonated') || $target->canBeImpersonated())
            && config('kda.filament-user.impersonation_enabled');
    }

    protected static function getNavigationGroup(): ?string
    {
      
           return   'filament-shield::filament-shield.nav.group' === __('filament-shield::filament-shield.nav.group') ? __("filament.navigation.settings") : 'filament-shield::filament-shield.nav.group';
           
    }


    public static function form(Form $form): Form
    {
        return $form
            ->schema([
                Tabs::make('Heading')
                    ->tabs([

                        Tabs\Tab::make('User')->schema([
                            Forms\Components\TextInput::make('name')
                                ->required()
                                ->maxLength(255),
                            Forms\Components\TextInput::make('email')
                                ->email()
                                ->required()
                                ->maxLength(255),
                            Forms\Components\TextInput::make('password')
                                ->password()
                                ->maxLength(255)
                                ->required(static fn (Page $livewire): bool => $livewire instanceof CreateUser)
                                ->dehydrated(static fn (null|string $state): bool => filled($state))
                                ->label(static fn (Page $livewire): string => ($livewire instanceof EditUser) ? 'New password' : 'Password'),
                            Forms\Components\Toggle::make('enabled')->default(true)->when(static fn():bool=> config('kda.filament-user.wimdy_enabled')),
                            Forms\Components\Toggle::make('force_password_change')->default(true)->when(static fn():bool=> config('kda.filament-user.wimdy_enabled')),
                        ]),
                        Tabs\Tab::make( __('filament-user::panel.roles'))->when(static fn():bool=> config('kda.filament-user.spatie_permissions_enabled'))
                            ->schema([
                                Forms\Components\MultiSelect::make('roles')->relationship('roles', 'name')->preload(true),
                            ]),
                        Tabs\Tab::make( __('filament-user::panel.permissions'))->when(static fn():bool=> config('kda.filament-user.spatie_permissions_enabled'))
                        ->schema([
                            
                            Forms\Components\MultiSelect::make('permissions')->relationship('permissions', 'name')->columns(2)->preload(true),
                        ]),
                    ]),

            ]);
    }

    public static function getActions():array{
        $actions = [
            Tables\Actions\EditAction::make(),
        ];
        if(class_exists(Impersonate::class)){
            $actions [] = Impersonate::make('impersonate')->hidden(static fn($record):bool=> !static::allowed(Filament::auth()->user(), $record));
        }
        return $actions;
    }

    public static function table(Table $table): Table
    {
        return $table
            ->columns([
                Tables\Columns\TextColumn::make('id')->toggleable(),
                Tables\Columns\TextColumn::make('name')->toggleable(),
                Tables\Columns\TextColumn::make('email')->toggleable(),
                BooleanColumn::make('enabled')->hidden(static fn():bool=> !config('kda.filament-user.wimdy_enabled')),
                BooleanColumn::make('active')->hidden(static fn():bool=> !config('kda.filament-user.wimdy_enabled'))
                    ->getStateUsing(fn ($record): bool => ($record->force_password_change && ! blank($record->password_changed_on) || ! $record->force_password_change))
                    ->trueColor(fn ($record): string => ! $record->force_password_change ? 'secondary' : 'success'),
                // BooleanColumn::make('force_password_change')->getStateUsing(fn ($record): bool => false ),
               
               
            ])
            ->filters([
                //
            ])
            ->actions(
                static::getActions()
            )
            ->bulkActions([
            ]);
    }

    public static function getRelations(): array
    {
        return [
            //

        ];
    }

    public static function getPages(): array
    {
        return [
            'index' => Pages\ListUsers::route('/'),
            'create' => Pages\CreateUser::route('/create'),
            'edit' => Pages\EditUser::route('/{record}/edit'),
        ];
    }
}
