<?php

namespace KDA\Filament\User\Resources\UserResource\Pages;

use KDA\Filament\User\Resources\UserResource;
use Filament\Resources\Pages\CreateRecord;

class CreateUser extends CreateRecord
{
    protected static string $resource = UserResource::class;
}
