<?php
namespace KDA\Filament\User;
use KDA\Laravel\PackageServiceProvider;
//use Illuminate\Support\Facades\Blade;
class ServiceProvider extends PackageServiceProvider
{
    use \KDA\Laravel\Traits\HasCommands;
    use \KDA\Laravel\Traits\HasConfig;
    use \KDA\Laravel\Traits\HasProviders;
    use \KDA\Laravel\Traits\HasDumps;
    use \KDA\Laravel\Traits\HasTranslations;

    protected $packageName="filament-user";
    
    protected $dumps=[
        'users',
        'role_has_permissions',
        'roles',
        'permissions',
        'model_has_roles',
        'model_has_permissions',
    ] ;
    /* This is mandatory to avoid problem with the package path */
    protected function packageBaseDir()
    {
        return dirname(__DIR__, 1);
    }
     // trait \KDA\Laravel\Traits\HasConfig; 
     //    registers config file as 
     //      [file_relative_to_config_dir => namespace]
    protected $configDir='config';
    protected $configs = [
         'kda/filament-user.php'  => 'kda.filament-user'
    ];
        //register filament provider
    protected $additionnalProviders=[
        \KDA\Filament\User\FilamentServiceProvider::class
    ];
    /*public function register()
    {
        parent::register();
    }*/
    /**
     * called after the trait were registered
     */
    public function postRegister(){
    }
    //called after the trait were booted
    protected function bootSelf(){
    }
}
