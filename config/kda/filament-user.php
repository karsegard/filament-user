<?php
// config for KDA/Filament\User
return [
    'wimdy_enabled'=>true,
    'spatie_permissions_enabled'=>true,
    'impersonation_enabled'=>true
];
